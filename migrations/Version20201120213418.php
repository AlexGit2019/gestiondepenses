<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201120213418 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_FCEC9EFFD02F13');
        $this->addSql('CREATE TEMPORARY TABLE __temp__personne AS SELECT id, evenement_id, nom, depenses FROM personne');
        $this->addSql('DROP TABLE personne');
        $this->addSql('CREATE TABLE personne (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, evenement_id INTEGER NOT NULL, nom VARCHAR(100) NOT NULL COLLATE BINARY, depenses DOUBLE PRECISION NOT NULL, parts INTEGER NOT NULL, CONSTRAINT FK_FCEC9EFFD02F13 FOREIGN KEY (evenement_id) REFERENCES evenement (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO personne (id, evenement_id, nom, depenses) SELECT id, evenement_id, nom, depenses FROM __temp__personne');
        $this->addSql('DROP TABLE __temp__personne');
        $this->addSql('CREATE INDEX IDX_FCEC9EFFD02F13 ON personne (evenement_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX IDX_FCEC9EFFD02F13');
        $this->addSql('CREATE TEMPORARY TABLE __temp__personne AS SELECT id, evenement_id, nom, depenses FROM personne');
        $this->addSql('DROP TABLE personne');
        $this->addSql('CREATE TABLE personne (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, evenement_id INTEGER NOT NULL, nom VARCHAR(100) NOT NULL, depenses DOUBLE PRECISION NOT NULL)');
        $this->addSql('INSERT INTO personne (id, evenement_id, nom, depenses) SELECT id, evenement_id, nom, depenses FROM __temp__personne');
        $this->addSql('DROP TABLE __temp__personne');
        $this->addSql('CREATE INDEX IDX_FCEC9EFFD02F13 ON personne (evenement_id)');
    }
}
