<?php

namespace App\Form;

use App\Entity\Personne;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class,['label' => "Nom de la personne",'attr' => ['placeholder'=>"Nom de la personne"]])
            ->add("depenses",TextType::class, ['label' => "Depenses",'attr' => ['placeholder'=>"Depenses..."]])
            ->add("parts",TextType::class, ['label' => "Parts",'attr' => ['placeholder'=>"Parts..."]])
            ->add("ok", SubmitType::class,['label'=>"Valider la personne"])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Personne::class,
        ]);
    }
}
