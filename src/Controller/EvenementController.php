<?php

namespace App\Controller;

use App\Entity\Evenement;
use App\Entity\Personne;
use App\Form\EvenementSupprimerType;
use App\Form\EvenementType;
use App\Form\PersonneSupprimerType;
use App\Form\PersonneType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class EvenementController extends AbstractController
{
    /**
     * @Route("/", name="evenements")
     */
    public function index()
    {
        $repo = $this->getDoctrine()->getRepository(Evenement::class);
        $evenements = $repo->findAll();
        return $this->render('Evenement/index.html.twig', [
            "evenements" => $evenements
        ]);
    }

    /**
     * @Route("/ajouterEvenement", name="ajouterEvenement")
     */
    public function ajouter(Request $request)
    {
        $evenement = new Evenement();
        $form = $this->createForm(EvenementType::class, $evenement);
        $modele = [
            "formulaire" => $form->createView()
        ];
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($evenement);
            $em->flush();

            return $this->redirectToRoute("evenements");
        }
        return $this->render("Evenement/ajouter.html.twig",$modele);
    }
    /**
     * @Route("/modifierEvenement/{id}",name="modifierEvenement")
     */
    public function modifier($id, Request $request)
    {
        //Aller chercher l'évènement dans la BDD
        $repo=$this->getDoctrine()->getRepository(Evenement::class);
        $evenement = $repo->find($id);

        //création du formulaire
        $form = $this->createForm(EvenementType::class, $evenement);

        //récupération du POST
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //récupération de l'entityManager
            $em = $this->getDoctrine()->getManager();

            //dire au manager qu'on veut garder notre objet en BDD
            $em->persist($evenement);

            //générer l'update
            $em->flush();

            //aller à la liste des catégories
            return $this->redirectToRoute("evenements");
        }

        return $this->render("Evenement/modifier.html.twig", [
            "formulaire" => $form->createView()
        ]);
    }
    /**
     * @Route("/supprimerEvenement/{id}",name="supprimerEvenement")
     */
    public function supprimer($id, Request $request)
    {
        //Aller chercher la catégorie dans la BDD
        $repo=$this->getDoctrine()->getRepository(Evenement::class);
        $evenement = $repo->find($id);

        //création du formulaire
        $form = $this->createForm(EvenementSupprimerType::class, $evenement);

        //récupération du POST
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //récupération de l'entityManager
            $em = $this->getDoctrine()->getManager();

            //dire au manager qu'on veut garder notre objet en BDD
            $em->remove($evenement);

            //générer l'update
            $em->flush();

            //aller à la liste des catégories
            return $this->redirectToRoute("evenements");
        }

        return $this->render("Evenement/supprimer.html.twig", [
            "formulaire" => $form->createView(),
            "evenement"=>$evenement
        ]);
    }
    /**
     * @Route("/pageAccueilEvenement/{id}", name="accueilEvenement")
     */
    public function accueilEvenement($id)
    {
        $repo = $this->getDoctrine()->getRepository(Evenement::class);
        $evenement = $repo->find($id);
        return $this->render("Evenement/evenement.html.twig",[
            "evenement" => $evenement
        ]);
    }

    /**
     * @Route("/ajouterPersonne/{id}", name="ajouterPersonne")
     */

    public function ajouterPersonne($id, Request $request)
    {
        $personne = new Personne();
        $repo = $this->getDoctrine()->getRepository(Evenement::class);
        $evenement = $repo->find($id);
        $form = $this->createForm(PersonneType::class, $personne);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $personne->setEvenement($evenement);
            $em = $this->getDoctrine()->getManager();
            $em->persist($personne);
            $em->flush();
            return $this->redirectToRoute("accueilEvenement",["id" => $id]);
        }
        return $this->render("personne/ajouter.html.twig", [
            "formulaire" =>$form->createView(),
            "idEvenement" => $id
        ]);

    }
    /**
     * @Route("/modifierPersonne{idPersonne}/{idEvenement}", name="modifierPersonne")
     */
    public function modifierPersonne($idEvenement, $idPersonne, Request $request)
    {
        $repo = $this->getDoctrine()->getRepository(Personne::class);
        $personne = $repo->find($idPersonne);
        $repo2 = $this->getDoctrine()->getRepository(Evenement::class);
        $evenement = $repo2->find($idEvenement);
        $form= $this->createForm(PersonneType::class, $personne);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($personne);
            $em->flush();
            return $this->redirectToRoute("accueilEvenement",["id" => $idEvenement]);
        }
        return $this->render("personne/modifier.html.twig",[
            "formulaire" => $form->createView(),
            "personne" => $personne,
            "idEvenement"=> $idEvenement
        ]);
    }
    /**
     * @Route("/supprimerPersonne/{idPersonne}/{idEvenement}", name="supprimerPersonne")
     */
    public function supprimerPersonne($idEvenement, $idPersonne, Request $request)
    {
        $repo = $this->getDoctrine()->getRepository(Personne::class);
        $personne = $repo->find($idPersonne);
        $repo2 = $this->getDoctrine()->getRepository(Evenement::class);
        $evenement = $repo2->find($idEvenement);
        $form= $this->createForm(PersonneSupprimerType::class, $personne);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->remove($personne);
            $em->flush();
            return $this->redirectToRoute("accueilEvenement",["id" => $idEvenement]);
        }
        return $this->render("personne/supprimer.html.twig",[
            "formulaire" => $form->createView(),
            "personne"=>$personne,
            "idEvenement" => $idEvenement
        ]);
    }
    /**
     * @Route("/calculerMontant/{idEvenement}", name="calculerMontant")
     */

    public function calculerMontant($idEvenement)
    {
        $totalDepenses = 0;
        $totalParts = 0;
        $repo = $this->getDoctrine()->getRepository(Evenement::class);
        $evenement = $repo->find($idEvenement);
        $personnesEvenement = $evenement->getPersonnesEvenement();
        foreach ($personnesEvenement as $value )
        {
            $depensesPersonne = $value->getDepenses();
            $partsPersonne = $value->getParts();
            $totalDepenses += $depensesPersonne;
            $totalParts += $partsPersonne;
        }
        $montantPart = $totalDepenses / $totalParts;
        $dictionnairePersonnesDepenses = [];
        foreach ($personnesEvenement as $value)
        {
            $partsPersonne = $value->getParts();
            $depensesPersonne = $value->getDepenses();
            $bilanDepenses = $depensesPersonne - ($montantPart * $partsPersonne);
            $dictionnairePersonnesDepenses[$value->getNom()] = $bilanDepenses;
        }
        return $this->render("Evenement/evenement.html.twig",[
            "id" => $idEvenement,
            "dictionnaire" => $dictionnairePersonnesDepenses,
            "evenement" => $evenement
        ]);
    }
}
