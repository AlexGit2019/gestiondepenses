<?php

namespace App\Entity;

use App\Repository\PersonneRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PersonneRepository::class)
 */
class Personne
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $nom;

    /**
     * @ORM\ManyToOne(targetEntity=Evenement::class, inversedBy="personnesEvenement")
     * @ORM\JoinColumn(nullable=false)
     */
    private $evenement;
    
    /**
     * @ORM\Column(type="float", length=255)
     */
    private $depenses;
    /**
     * @ORM\Column(type="integer", length=2)
     */
    private $parts;

    public function getParts(): ?int
    {
        return $this->parts;
    }

    public function setParts(int $value): ?self
    {
        $this->parts = $value;
        return $this;
    }

    
    public function getDepenses(): ?float
    {
        return $this->depenses;
    }
    
    public function setDepenses(float $value): ?self
    {
        $this->depenses = $value;
        return $this;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getEvenement(): ?Evenement
    {
        return $this->evenement;
    }

    public function setEvenement(?Evenement $evenement): self
    {
        $this->evenement = $evenement;

        return $this;
    }
}
