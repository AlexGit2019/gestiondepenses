<?php

namespace App\Entity;

use App\Repository\EvenementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EvenementRepository::class)
 */
class Evenement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity=Personne::class, mappedBy="evenement")
     */
    private $personnesEvenement;

    public function __construct()
    {
        $this->personnesEvenement = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection|Personne[]
     */
    public function getPersonnesEvenement(): Collection
    {
        return $this->personnesEvenement;
    }

    public function addPersonnesEvenement(Personne $personnesEvenement): self
    {
        if (!$this->personnesEvenement->contains($personnesEvenement)) {
            $this->personnesEvenement[] = $personnesEvenement;
            $personnesEvenement->setEvenement($this);
        }

        return $this;
    }

    public function removePersonnesEvenement(Personne $personnesEvenement): self
    {
        if ($this->personnesEvenement->contains($personnesEvenement)) {
            $this->personnesEvenement->removeElement($personnesEvenement);
            // set the owning side to null (unless already changed)
            if ($personnesEvenement->getEvenement() === $this) {
                $personnesEvenement->setEvenement(null);
            }
        }

        return $this;
    }
}
